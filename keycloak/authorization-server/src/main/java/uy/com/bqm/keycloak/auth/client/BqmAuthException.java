package uy.com.bqm.keycloak.auth.client;

import javax.ws.rs.core.Response.Status;

import org.keycloak.services.ErrorResponseException;

public class BqmAuthException extends ErrorResponseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BqmAuthException(String error, String errorDescription, Status status) {
		super(error, errorDescription, status); 
	}

}
