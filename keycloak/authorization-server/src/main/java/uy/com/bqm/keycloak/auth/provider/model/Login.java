package uy.com.bqm.keycloak.auth.provider.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor(force = true)
@AllArgsConstructor 
public class Login implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String user;
	private String password;

}


